Override admin method

public function edit()
{
	$params = parent::edit();
	//do stuff

	//add widgets
	$this->useWidget( 'richtext' );
	$this->useWidget( 'tabs' );
	$this->useWidget( 'autocomplete' );
	$this->useWidget( 'sortable' );


	return $params;
}